const getValue=function(target,key,_def){
     return (target[key]===undefined) ?  _def:target[key]
}
const isEmpty=function(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
const cloneObject = (object) => JSON.parse(JSON.stringify(object));

export {getValue,isEmpty,cloneObject}
