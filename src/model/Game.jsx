import {Place,NORMAL,positions} from "./Place";
import {getValue} from "../Utils";



class Game {

    constructor(){

        this.places=[]
            for (var i=0; i < 64; i++) {
            this.places[i] = new Place(i, getValue(positions, i, NORMAL));
        }
        this.log = ""

        this.started=false
        this.currentGame=1
        this.wins=false
    }



}

export { Game}
