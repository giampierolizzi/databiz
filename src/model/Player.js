class Player {
    username: strimg
    markerColor: string
    position: number
    prevPosition: number
    lastGame: number

    constructor(username,markerColor=null){
        this.username=username
        this.markerColor=markerColor
        this.position= 0
        this.prevPosition=0
        this.lastGame=0
    }
}

export default Player;
