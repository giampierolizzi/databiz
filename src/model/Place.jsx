const GOOVE=2
const BRIDGE=1
const NORMAL=0
const START=-1

const WIN_POS=63

const positions={0:START, 12:BRIDGE,5:GOOVE, 9:GOOVE, 14:GOOVE, 18:GOOVE, 23:GOOVE, 27:GOOVE, 63:WIN_POS}

class Place{
   id : number;
   type: number;
   players: string[];



    constructor(id,type=NORMAL){
        this.type=type;
        this.id=id
        this.isGOOVE= function(){ return this.type === GOOVE};
        this.isBRIDGE= function(){ return this.type === BRIDGE};
        this.isNORMAL= function(){ return this.type === NORMAL};
        this.players=[]
        this.hasPlayer= function(){ return this.players.length>0 }
    }


}


export {Place,GOOVE,BRIDGE,NORMAL,START,WIN_POS,positions}
