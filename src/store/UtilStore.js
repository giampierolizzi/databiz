import {getValue} from "../Utils";

const SimpleReducer=(type,initState=null)=>
    (state=initState,action)=> (action.type === type)? action.payload:state

const ComplexReducer= function (actions,initState=null) {
    return function (state = initState, action) {
        return getValue(actions, action.type, (state, action) => state)(state, action)
    }
}

const SimpleAction = function (type,_def=null){
    return function(payload=_def){
        return {type: type, payload}
    }
}

export {SimpleReducer,ComplexReducer,SimpleAction}
