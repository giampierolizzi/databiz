import {combineReducers} from "redux";
import {GameReducer} from "./GameReducer";
import {PlayerReducer} from "./PlayerReducer";
import {DiceReducer} from "./DiceReducer";

const reducers = combineReducers({
    game:GameReducer,
    players:PlayerReducer,
    dice: DiceReducer
})

export default reducers
