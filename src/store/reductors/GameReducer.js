import {ComplexReducer} from "../UtilStore";
import {Game} from "../../model/Game";
import {cloneObject} from "../../Utils";



const GameReducer= ComplexReducer({
        "CHANGE_GAME_STATE": (state,action) => action.payload,
        "START_GAME": (state,action) => {
            var currState=  cloneObject(state)
            currState.started=action.payload.start
            currState.places[0].players=action.payload.players
            for(var i=1;i<64;i++){
                currState.places[i].players=[]
            }
            return currState
        },
        "USER_WINS": (state,action) => {
            var currState=  Object.assign({}, state)
            currState.wins=true
            return currState
        },
        "GAME_MOVE": (state,action) => {
            var currState=  Object.assign({}, state)
            var places=  cloneObject(state.places)
            var from=action.payload.from
            var to=action.payload.to
            var username=action.payload.username
            var idx=places[from].players.findIndex(pname=>pname===username)
            if(idx>-1) places[from].players.splice(idx,1)
            places[to].players.push(username)
            currState.places=places
            return currState
        },
        "GAME_LOG": (state,action) => {
            var currState=  Object.assign({}, state)
            currState.log=action.payload.trim()
            return currState
        },
        "NEXT_GAME": (state,action) => {
            var currState=  Object.assign({}, state)
            currState.currentGame++
            return currState

        },
        "NEW_GAME": (state,action) => {
            return new Game()
        },
    }
    ,
    new Game()
)
export {GameReducer}
