import {ComplexReducer} from "../UtilStore";
import Player from "../../model/Player";
import {cloneObject} from "../../Utils";


const PlayerReducer= ComplexReducer({
        "CHANGE_PLAYER_LIST": (state,action) => {
            return action.payload
        },
        "SAVE_PLAYER": (state,action) => {
            var players=  Object.assign({}, state)
            var username=action.payload
            players[username]=new Player(username)
            return players
        },
        "REMOVE_PLAYER": (state,action) => {
            var players=  Object.assign({}, state)
            var username=action.payload
            delete players[username]
            return players
        },
        "PLAYER_PLAYES": (state,action) => {
            var players=  Object.assign({}, state)
            var params=action.payload
            var player=players[params.username]
            player.prevPosition=player.postion
            player.position=params.position
            return players

        },
        "PLAYER_MOVES": (state,action) => {
            var players=  Object.assign({}, state)
            var param=action.payload
            var player=players[param.username]
            player.prevPosition=player.position
            player.position=param.position
            return players
        },
        "NEXT_PLAYER": (state,action) => {
            var players=  Object.assign({}, state)
            var username=action.payload
            var player=players[username]
            player.lastGame++
            return players

        },
        "PLAYER_NEW_GAME": (state,action) => {
            var players=  cloneObject(state)
            for (let player of Object.values(players)) {
              player.lastGame=0
              player.prevPosition=0
              player.position=0
            }
            return players

        },




    }
,{})

export {PlayerReducer}
