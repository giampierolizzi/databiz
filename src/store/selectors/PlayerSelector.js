import { createSelector } from 'reselect'

const getCurrentGame = (state, props) => state.game.currentGame
const getPlayers = (state, props) => state.players
const getCurrentPlayer=(currentGame, players) => {
    let arr=Object.entries(players)
    let idx=arr.findIndex(p => {
        return p[1].lastGame<currentGame
    })
    if(idx<0) return null;
    return arr[idx][1];
}


const makeGetCurrentPlayer = () => {
    return createSelector(
        [ getCurrentGame, getPlayers ],
        getCurrentPlayer
    )
}

export {makeGetCurrentPlayer,getCurrentPlayer}
