import {SimpleAction} from "../UtilStore";

const changeGameState= SimpleAction("CHANGE_GAME_STATE")
const startGame= SimpleAction("START_GAME")
const userWins= SimpleAction("USER_WINS")

const gameMove= SimpleAction("GAME_MOVE")
const nextGame= SimpleAction("NEXT_GAME")
const newGame= SimpleAction("NEW_GAME")
const gameLog= SimpleAction("GAME_LOG")

export {changeGameState,startGame,userWins,gameMove,nextGame,newGame,gameLog}
