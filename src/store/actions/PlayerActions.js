import {SimpleAction} from "../UtilStore";

const savePlayer= SimpleAction("SAVE_PLAYER")
const removePlayer= SimpleAction("REMOVE_PLAYER")
const playerPlays= SimpleAction("PLAYER_PLAYES")
const playerMove= SimpleAction("PLAYER_MOVE")
const nextPlayer= SimpleAction("NEXT_PLAYER")
const playerNewGame=SimpleAction("PLAYER_NEW_GAME")
const clearPlayerList=SimpleAction("CHANGE_PLAYER_LIST",{})
export {savePlayer,removePlayer,playerPlays,playerMove,nextPlayer,playerNewGame,clearPlayerList}
