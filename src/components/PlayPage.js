import React from "react";
import {connect, ReactReduxContext} from "react-redux"
import {changeDice} from "../store/actions/DiceActions";
import Dice from   "./Dice"
import {makeGetCurrentPlayer,getCurrentPlayer} from "../store/selectors/PlayerSelector"
import GameService from "../servicers/GameService";
import PlaceList from "./PlaceList";
import {nextPlayer} from "../store/actions/PlayerActions";
import {newGame} from "../store/actions/GameActions";

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max + 1 - min)) + min;
}

class PlayPage extends React.Component {
    static contextType = ReactReduxContext;

    constructor(props){
        super(props)
        this.handleNextPlayer = this.handleNextPlayer.bind(this);
        this.handleRoll = this.handleRoll.bind(this);
        this.isGameEnded=this.isGameEnded.bind(this)
        this.hasAnyOtherPlayer=this.hasAnyOtherPlayer.bind(this)
        this.handleEndGame=this.handleEndGame.bind(this)
        this.state = {
            rolled: false
        };



    }

    handleRoll(event) {
        event.preventDefault();
        event.stopPropagation();
        var gameService= new GameService(this.context.store)

        let dice=[getRandomInt(1,6),getRandomInt(1,6)]
        this.props.rolls(dice)
        gameService.rolls(this.props.currentPlayer,dice[0]+dice[1])
        this.setState({rolled:true})
    }

    handleNextPlayer(event){
        event.preventDefault();
        event.stopPropagation();
        this.setState({rolled:false})
        let userName=this.props.currentPlayer.username
        var gameService= new GameService(this.context.store)
        gameService.nextPlayer(userName)
    }

    handleEndGame(event){
        event.preventDefault();
        event.stopPropagation();
        if(window.confirm("Quit game?")) {
            this.setState({rolled: false})
            var gameService= new GameService(this.context.store)
            gameService.newGame()
        }
    }

    isGameEnded(){
       return (this.props.games.wins || ! this.hasAnyOtherPlayer())
    }

    hasAnyOtherPlayer(){
        return getCurrentPlayer(this.props.game.currentGame,this.props.players)!=null
    }


    render() {
        return (
            <div>
                <div>
                    <Dice/>
                    <p>
                        Game n. {this.props.game.currentGame}
                    </p>
                    <p>
                        Player {this.props.currentPlayer.username} {this.props.game.log===""?"": ": "+this.props.game.log}
                    </p>
                    {(!this.state.rolled && !this.props.game.wins) ? <button onClick={this.handleRoll}>Roll</button>:''}
                    {(this.state.rolled && this.hasAnyOtherPlayer() && !this.props.game.wins)? <button onClick={this.handleNextPlayer}>NextPlayer</button>:""}
                    <button onClick={this.handleEndGame}>Quit game</button>
                </div>
                <PlaceList/>
            </div>
        );
        }



}

const currentPlayer=makeGetCurrentPlayer()



const mapStateToProps = (state, props) => {
    return {
        players: state.players,
        game: state.game,
        currentPlayer: currentPlayer(state)
    }
}


const mapDispatchToProps = dispatch => {
    return {
        rolls: values => dispatch(changeDice(values)),
        nextPlayer: (name)=> dispatch(nextPlayer(name))
    }
}




export default connect(mapStateToProps,mapDispatchToProps,null)(PlayPage)
