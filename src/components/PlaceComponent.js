import React from "react";
import {positions,GOOVE,BRIDGE,NORMAL,WIN_POS,START} from "../model/Place"


var placeType={};
placeType[START]=": Start"
placeType[GOOVE]=": The Goove"
placeType[BRIDGE]=": The Bridge"
placeType[NORMAL]=""
placeType[WIN_POS]=": End"





class PlaceComponent extends React.Component {

    render() {
        const divStyle = {
            margin: '5px',
            border: '3px solid black'
        };
        let players= this.props.players.map(usr => {
            return (
                <li>
                    {usr}
                </li>
            )
        })

        return (
            <div style={divStyle}>
                <p>{this.props.id} {placeType[positions[this.props.id]]}</p>
                {players.length>0?<p>Players:</p>:<></>}
                <ul>
                    {players}
                </ul>
            </div>
        );
    }
}

export default PlaceComponent;
