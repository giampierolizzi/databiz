import React from "react";
import {connect} from "react-redux"
import PlayerComponent from "./PlayerComponent"
import {clearPlayerList, savePlayer} from "../store/actions/PlayerActions";
import {isEmpty} from "../Utils";
import {startGame} from "../store/actions/GameActions";


class PlayerList extends React.Component {
    constructor(props){
        super(props)
        this.handleSave = this.handleSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleClear = this.handleClear.bind(this);
        this.state = {
            name:"",
        };
    }

    handleChange(event) {
        this.setState({name: event.target.value});
    }

    handleSave(event){
        event.preventDefault();
        event.stopPropagation();

        let players=this.props.players
        let username=this.state.name.trim()

        if( ! (players[username]=== undefined)){
            window.alert(username+" already existing player.")
            return
        }

        this.props.savePlayer(username)
        this.setState({name:""})

    }

    handleClear(event){
        event.preventDefault();
        event.stopPropagation();

        if(window.confirm("Clear Player List ?")) this.props.clear()

    }


    handleStart(event){
        event.preventDefault();
        event.stopPropagation();


        this.props.startGame( Object.entries(this.props.players).map((s)=> s[0]))

    }



    render() {
        let players=this.props.players
        let playerlist=[]
        let tmp=Object.entries(players).sort((a,b)=>a[0].localeCompare(b[0]))
        playerlist = tmp.map(entry => {
            return (<PlayerComponent username={entry[0]} />)
        })
        return (
            <div>
                <form>
                    <label>
                        Player name:
                        <input type="text" name="name" onChange={this.handleChange} value={this.state.name}/>
                    </label>

                    {this.state.name.trim()===""?<></>:<button name="name" onClick={this.handleSave} >Add Player</button>}
                    {isEmpty(players)?<></>:<button name="name" onClick={this.handleClear} >Clear</button>}
                    {isEmpty(players)?<></>:<button name="name" onClick={this.handleStart} >Start Game</button>}

                </form>
                <table>
                    <thead>
                    <tr>
                        <th>
                            Player
                        </th>
                        <th>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {playerlist}
                    </tbody>
                </table>

            </div>

        );
    }


}

const mapStateToProps= state => {
    return { players: state.players }
}
const mapDispatchToProps = (dispatch,props) => {
    return {
        // dispatching plain actions
        savePlayer: username => {
                   return dispatch(savePlayer(username))

        },
        startGame: (players) => {
             dispatch(startGame({start:true,players: players}))

        },
        clear: () => {
            dispatch(clearPlayerList({}))
        },


    }
}




export default connect(mapStateToProps,mapDispatchToProps,null)(PlayerList)
