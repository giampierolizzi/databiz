import React from "react";
import {connect} from "react-redux"
import {isEmpty} from "../Utils";
import PlayPage from "./PlayPage";
import PlayerList from "./PlayerList";



class MainPage extends React.Component {


    render() {
        return (
            <div>
                <div>
                    { (isEmpty(this.props.players) || ! this.props.started) ?<PlayerList/>: <PlayPage/>}
                </div>
            </div>
        );
    }


}

const mapStateToProps = state => {
    return {
        players: state.players,
        started: state.game.started,
    }
}

export default connect(mapStateToProps,null,null)(MainPage)
