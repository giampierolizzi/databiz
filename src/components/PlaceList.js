import React from "react";
import {connect} from "react-redux";
import Place from "./PlaceComponent"


class PlaceList extends React.Component {

    render() {
        let places= this.props.places.map(place => {
            return (
                <Place id={place.id} players={place.players}/>
            )
        })

        return (
            <div>
                {places}
            </div>
        );
    }
}

const mapStateToProps= state => {
    return { places: state.game.places }
}




export default connect(mapStateToProps,null,null)(PlaceList)

