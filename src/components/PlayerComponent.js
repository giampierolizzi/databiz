import React from "react";
import {connect} from "react-redux"
import {removePlayer} from "../store/actions/PlayerActions"

class PlayerComponent extends React.Component {
    constructor(props){
        super(props)
        this.handleRemove= this.handleRemove.bind(this)
    }
    handleRemove(event){
        event.preventDefault();
        event.stopPropagation();
        if(window.confirm("Player "+this.props.username+ " remove?")){
            this.props.removePlayer(this.props.username)
        }
    }
    render() {
        return (
            <tr>
                <td>{this.props.username}</td>
                <td><button onClick={this.handleRemove}>Remove</button></td>
            </tr>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {

        removePlayer: username => dispatch(removePlayer(username)),

    }
}

export default connect(null,mapDispatchToProps,null)(PlayerComponent)
