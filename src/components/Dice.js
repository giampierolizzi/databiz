import React from "react";
import {connect} from "react-redux"
import "../resources/dice.css"


class Die extends React.Component {
    render(){
        let className="dice dice-"+((this.props.num===0)?"1":this.props.num.toString())
        return (<span  style={{"font-size": "2em"}}><span className={className}/></span>)
    }
}

class Dice extends React.Component {
    render(){
       return (
           <div>
               <p>
               <Die num={this.props.dice[0]}/>
               <Die num={this.props.dice[1]}/>
               </p>
           </div>)

    }
}
const mapStateToProps= state => {
    return { dice: state.dice }
}
export default connect(mapStateToProps)(Dice)

