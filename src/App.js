import React from 'react';
import './App.css';
import { Provider } from 'react-redux'
import MainPage from "./components/MainPage";
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';


class App  extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hasError: false,
            error:"",
            info:""
        }
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={<div>Loading state ....</div>} persistor={persistor}>
                    <div className="App">
                        <MainPage/>
                    </div>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;
