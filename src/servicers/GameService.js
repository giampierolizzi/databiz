import {Place,GOOVE,BRIDGE,WIN_POS} from "./../model/Place";
import {nextPlayer, playerMove, playerPlays,playerNewGame} from "../store/actions/PlayerActions";
import {userWins, gameMove, gameLog, nextGame,newGame} from "../store/actions/GameActions";
import {getCurrentPlayer} from "../store/selectors/PlayerSelector";
import {cloneObject} from "../Utils";


function move(dispatch,places:Place[],username:string,oldPosition:number,newPosition:number,log:string){
    var players=places[newPosition].players

    if(oldPosition===newPosition){
        dispatch(gameLog(log))
        return log
    }
    if(players.length >0 ) {
        log += " On "+newPosition+" there is "+players[0]+", who returns to "+ oldPosition
        dispatch(gameMove({from:newPosition,to: oldPosition,username:players[0]}))
        dispatch(playerMove({username: players[0],position:oldPosition}))

    }

    dispatch(playerPlays({username: username,position:newPosition}))
    dispatch(gameMove({from:oldPosition,to: newPosition,username:username}))
    dispatch(gameLog(log))
    return log

}


class GameService {
    constructor(store){
        this.store=store
    }

    newGame(){
        var dispatch=this.store.dispatch
        dispatch(playerNewGame())
        dispatch(newGame())
    }

    nextPlayer(userName){
        var state =this.store.getState()
        var dispatch=this.store.dispatch
        var  game = state.game
        var  players=cloneObject(state.players);
        players[userName].lastGame++;

        if(getCurrentPlayer(game.currentGame,players)===null) {
            dispatch(nextGame())
        }
        dispatch(nextPlayer(userName))
        dispatch(gameLog(""))
    }

     rolls(player:Player, rolls:number) {

        if (rolls > 12 && rolls < 1) {
            return ""
        }
        var state =this.store.getState()
        var dispatch=this.store.dispatch
        var  game = state.game
         var oldPosition = player.position
         var newPosition = oldPosition + rolls
         var newPlaces = game.places

         var log = game.log
         let username= player.username

        if (newPosition === WIN_POS) {
            log = username + " moves from " + oldPosition + " to " + WIN_POS + ". " + username + " wins!"
            dispatch(userWins(username))
            return move(dispatch,newPlaces, username, oldPosition, newPosition, log)
        }

        log = username + " moves from " + (oldPosition===0? "Start":oldPosition) + " to " + (oldPosition + rolls)

        if (newPosition > WIN_POS) {
            newPosition = WIN_POS - (newPosition - WIN_POS)
            log = username + " moves from " + oldPosition + " to " + WIN_POS + ". "+ username + " bonces! " + username + " returns to " + newPosition
        }

        var newPlace = newPlaces[newPosition]
        switch (newPlace.type) {
            case GOOVE: {
                newPosition = oldPosition + 2 * rolls
                log += ", the Groove. " + username + " moves again and goes to " + newPosition

                break
            }
            case BRIDGE: {
                newPosition = 12
                log = player + " moves from " + oldPosition + " to the Bridge. " + username + " jumps to " + newPosition

                break
            }
            default:
        }
        log += "."
         return move(dispatch,newPlaces, username, oldPosition, newPosition, log)
    }
};

export default GameService
